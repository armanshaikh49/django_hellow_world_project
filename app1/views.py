from django.shortcuts import render

# Create your views here.
#improting http
from django.http import HttpResponse

# home funtion creating
def home(request):
    return render(request, 'a.html',{'titles':'Django','link':'http://127.0.0.1:8000/'})

#crating profile funtion
def profile(request):
    return render(request, 'a.html',{'titles':'profiller page','link':'http://127.0.0.1:8000/'})
#adding two value on submit button feating the data form html7
def expression (request):
    a=int(request.POST["text1"])
    b=int(request.POST["text2"])
    c=a+b
    return render(request,'output.html',{'result':c})